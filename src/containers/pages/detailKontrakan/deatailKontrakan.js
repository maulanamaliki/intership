import React, { Component, Fragment } from 'react'
import Header from '../../../components/header/header'
import Footer from '../../../components/footer/footer'

const detailKontrakan = () => {
    return (
        <Fragment>
            <Header />

            <Footer />
        </Fragment>
    );
}

export default detailKontrakan;