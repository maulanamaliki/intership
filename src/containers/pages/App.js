import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from '../pages/home/Home'
import Properti from '../pages/properti/properti'
import Login from '../pages/login/login'
import Register from '../pages/register/register'
import About from '../pages/about/about'
import DetailKontrakan from '../pages/detailKontrakan/deatailKontrakan'

function noMatch() {
  return <h1>404, Page Not Found</h1>
}

function App() {
  return (
    <Router>
      <Fragment>
        <Switch>
          <Route exact path="/" exact component={Home} />
          <Route path="/properti" component={Properti} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/about" component={About} />
          <Route path="/detail" component={DetailKontrakan} />
          <Route component={noMatch} />
        </Switch>
      </Fragment>
    </Router >
  );
}

export default App;
