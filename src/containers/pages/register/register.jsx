import React, { Component, Fragment } from 'react';
import axios from 'axios';

class register extends Component {
    state = {
        form: {
            nama: '',
            email: '',
            password: '',
            nohp: ''
        }
    }

    handleChange = (event) => {
        let formNew = { ...this.state.form };
        formNew[event.target.name] = event.target.value;
        this.setState({
            form: formNew
        }, () => {
            console.log('state', this.state.form)
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        axios.post('http://localhost/dana-kontrakan-master/register/', this.state.form)
            .then(
                (response) => { console.log(response) },
                (error) => { console.log(error) }
            )
    }

    render() {
        return (
            <Fragment>

                <div className="simple-login-container">
                    <img src="img/logo.svg" alt="" />
                    <form>
                        <div className="contact2-form-title">
                            Register
                    </div>
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <label htmlFor="exampleInputEmail1">Username</label>
                                <input type="text" name="nama" className="form-control" placeholder="Nama" onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <label htmlFor="exampleInputEmail1">No. Hp</label>
                                <input type="text" name="nohp" className="form-control" placeholder="No. Hp" onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <label htmlFor="exampleInputEmail1">Email</label>
                                <input type="email" name="email" className="form-control" placeholder="Email" onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" name="password" className="form-control" placeholder="password" onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 form-group">
                                <button type="submit" className="btn btn-block btn-login" onClick={this.handleSubmit}>Daftar</button>
                            </div>
                        </div>
                    </form>
                    <div className="row">
                        <div className="col-md-12 text-center mt-5">
                            <span>Copyright 2020 | Dakon. All Rights Reserved.</span>
                        </div>
                    </div>
                </div>

            </Fragment>
        );
    }
}

export default register;