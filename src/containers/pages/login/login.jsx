import React, { Component, Fragment } from 'react'

import './login.css'
import { Link } from 'react-router-dom';

const login = () => {
    return (
        <Fragment>

            <div className="simple-login-container">
                <img src="img/logo.svg" alt="" />
                <form>
                    <div class="contact2-form-title">
                        Contact Us
                    </div>
                    <div className="row">
                        <div className="col-md-12 form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" />
                        </div>
                    </div>
                    <div className="row" style={{ transform: 'translateX(16px)' }}>
                        <div className="custom-control custom-checkbox my-1 mr-sm-2">
                            <input type="checkbox" className="custom-control-input" id="customControlInline" />
                            <label className="custom-control-label" htmlFor="customControlInline">Remember my preference</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <p>Don't have account ? <Link to="/register">Create account</Link></p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12 form-group">
                            <button type="submit" className="btn btn-block btn-login">Login</button>
                        </div>
                    </div>
                </form>
                <div className="row">
                    <div className="col-md-12 text-center mt-5">
                        <span>Copyright 2020 | Dakon. All Rights Reserved.</span>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default login;