import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { ReactSVG } from 'react-svg'

import Baner from '../../../components/baner/baner'
import RekomendasiHome from '../../../components/rekomendasiHome/rekomendasiHome'
import About from '../../../components/about/about'
import ListProduk from '../../../components/listSlider/listSlider'
import AlurDakon from '../../../components/alurDakon/alurDakon'
import Header from '../../../components/header/header'
import Footer from '../../../components/footer/footer'

import './home.css'

class Home extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <Baner />
                <RekomendasiHome />
                <About />
                <ListProduk />
                <AlurDakon />
                <Footer />
            </Fragment>
        );
    }
}

export default Home;