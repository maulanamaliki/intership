import React, { Component } from 'react';
import Title from '../../../../components/title/title'

import Produk from '../../../../components/rekomendasiHome/produk/produkRekomendasi'
import './produk.css'

const produk = () => {
    return (
        <div className="jumbotron jumbotron-fluid rek">
            <div className="container">
                <Title title="Filter pencarian" />
                <form>
                    <div className="form-row filterForm">
                        <div className="form-group col-md-2">
                            <label htmlFor="inputEmail4">Kota atau daerah</label>
                            <input type="email" className="form-control" id="inputEmail4" />
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="inputEmail4">Filter</label>
                            <input type="email" className="form-control" id="inputEmail4" />
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="inputEmail4">Fasilitas</label>
                            <input type="email" className="form-control" id="inputEmail4" />
                        </div>
                        <div className="form-group col-md-2">
                            <label htmlFor="inputEmail4">Urutkan</label>
                            <input type="email" className="form-control" id="inputEmail4" />
                        </div>
                        <div className="form-group col-md-2 rh">
                            <label htmlFor="inputEmail4">Rentan Harga</label>
                            <input type="email" className="form-control" id="inputEmail4" />
                            <span>-</span>
                            <input type="email" className="form-control" id="inputEmail4" />
                        </div>
                        <div className="form-group col-md-2">
                            <button type="submit" className="btn btn-primary">Sign in</button>
                        </div>
                    </div>
                </form>
                <div className="row justify-content-center">
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                    <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                </div>
            </div>
        </div>
    );
}

export default produk;