import React, { Component, Fragment } from 'react'

import BannerFil from '../../../components/bannerFil/BannerFil'
import Header from '../../../components/header/header'
import Footer from '../../../components/footer/footer'
import Produk from './produk/produk'

class Properti extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <BannerFil />
                <Produk />
                <Footer />
            </Fragment>
        );
    }
}

export default Properti;