import React, { Component, Fragment } from 'react';

import Header from '../../../components/header/header'
import Footer from '../../../components/footer/footer'

import './about.css'

const about = () => {
    return (
        <Fragment>
            <Header />
            <div className="jumbotron jumbotron-fluid baner about">
                <div className="container">
                    <div className="row justify-content-center align-items-center h-100">
                        <div className="text-center" style={{ color: '#fff' }}>
                            <h1>About Us</h1>
                            {/* <h3>Dapatkan infonya dan bisa langsung mengajukan cicilan dengan Dakon</h3> */}
                        </div>
                    </div>
                </div>
            </div>
            <div className="jumbotron jumbotron-fluid aboutHome">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="imageAboutUp" />
                            <div className="imageAboutUp" />
                        </div>
                        <div className="col-md-6">
                            <div className="since">
                                <h3 className="roboto-light">Remember The Past</h3>
                                <h1 className="roboto-medium">Dakon 2000</h1>
                                <p className="roboto-light">
                                    sebuah website yang dapat membantu anda untuk mencari kontrakan yang sesuai dengan anda inginkan.
                            </p>
                                <p className="roboto-light">
                                    Dengan mengunakan DAKON anda dapat mengajukan cicilan sesuai dengan dana yang anda miliki.
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    );
}

export default about;