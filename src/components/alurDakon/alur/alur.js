import React, { Component } from 'react'

const alur = (props) => {
    return (
        <div className="row justify-center align-center alurProps">
            <div className="col-md-6">
                <img src={"" + props.img} />
            </div>
            <div className="col-md-6">
                <h1><span>{props.num}</span>{props.title}</h1>
                <p>{props.detail}</p>
            </div>
        </div>
    );
}

alur.defaultProps = {
    num: "",
    title: "",
    detail: "",
}
export default alur;