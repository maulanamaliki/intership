import React, { Component } from 'react'

import Alur from './alur/alur'
import Title from '../title/title'

import './alurDakon.css'

class alurDakon extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid alur">
                <div className="container">
                    <Title title="Pengunaan cicilan dakon" subTitle="alur cicilan dakon" align="center" />
                    <Alur img="/img/vector/1.svg" num="1" title="Bingung cari kos atau kontrakan" />
                    <Alur img="/img/vector/2.svg" num="2" title="Daftar dan isi data diri anda" />
                    <Alur img="/img/vector/3.svg" num="3" title="pilih kontrakan yang anda inginkan" />
                    <Alur img="/img/vector/4.svg" num="4" title="Tunggu proses validasi pengajuan kontrakan" />
                    <Alur img="/img/vector/5.svg" num="5" title="Upload tagihan anda setiap bulan" />
                </div>
            </div>
        );
    }
}

export default alurDakon;