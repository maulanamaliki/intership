import React from 'react'
import Button from '../buttonDakon/buttonDakon'

import './about.css'
import { Link } from 'react-router-dom'

const about = () => {
    return (
        <div className="jumbotron jumbotron-fluid aboutContact">
            <div className="container text-center text-white">
                <div className="row h-100">
                    <div className="col-sm-12 my-auto">
                        <h1>Kami akan membantu anda setiap langkah</h1>
                        <p>Ingin tau lebih lanjut tentang kami</p>
                        <Link to="/about"><Button text="Contact Us" /></Link>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default about