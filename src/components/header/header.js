import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ReactSVG } from 'react-svg'

const Header = (props) => {
    return (
        <nav className="navbar ${ props.nav } nav-home navbar-expand-md navbar-dark fixed-top" >
            <div className="container">
                <a className="navbar-brand" href="#">
                    <ReactSVG src="img/logo.svg" />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav ml-auto">
                        <Link className="nav-item nav-link active" to="/">Home</Link>
                        <Link className="nav-item nav-link" to="/properti">Properti</Link>
                        <Link className="nav-item nav-link" to="#">Faq</Link>
                        <Link className="nav-item nav-link" to="/about">About US</Link>
                        <Link className="nav-item nav-link" to="/login">Login</Link>
                    </div>
                </div>
            </div>
        </nav >
    );
}


export default Header;