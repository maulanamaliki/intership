import React, { Component } from 'react'

import './BannerFil.css'

class BannerFil extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid baner">
                <div className="container">
                    <div className="row justify-content-center align-items-center h-100">
                        <div className="text-center" style={{ color: '#fff', transform: 'translateY(4rem)' }}>
                            <h1>Cari kos yang bisa melakukan cicilan?</h1>
                            <h3>Dapatkan infonya dan bisa langsung mengajukan cicilan dengan Dakon</h3>
                        </div>
                        <form className="filter">
                            <div className="form-row align-items-center">
                                <div className="col-11">
                                    <label className="sr-only" htmlFor="inlineFormInput">Name</label>
                                    <input type="text" className="form-control" id="inlineFormInput" placeholder="Jane Doe" />
                                </div>
                                <div className="col-auto">
                                    <button type="submit" className="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        );
    }
}

export default BannerFil;