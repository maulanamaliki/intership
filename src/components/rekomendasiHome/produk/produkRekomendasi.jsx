import React from 'react';

const produkRekomendasi = (props) => {
    return (
        <div className="col-md-4">
            <div className="card">
                <img src={"" + props.img} className="card-img-top rekomendasi-img" />
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    <p className="card-text">
                    </p><div className="info-rek"><i className="fas fa-map-marker-alt fa-fw" /> {props.lokasiTempat}</div>
                    <div className="info-rek"><i className="fas fa-home fa-fw" /> {props.namaTempat}</div>
                    <p />
                    <div className="rekomendasi"><i className="fas fa-medal fa-fw" /></div>
                </div>
            </div>
        </div>
    );
}

export default produkRekomendasi;