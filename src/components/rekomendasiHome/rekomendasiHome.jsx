import React, { Component } from 'react'

import Title from '../title/title'
import Produk from '../rekomendasiHome/produk/produkRekomendasi'
import Button from '../buttonDakon/buttonDakon'

import './rekomendasiHome.css';
import { Link } from 'react-router-dom';

class rekomendasiHome extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid rek">
                <div className="container">
                    <Title title="Patnership dengan dakon" subTitle="Daftar unggulan rekomendasi dakon" />
                    <div className="row justify-content-center">
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Produk img="img/background/img5.jpg" title="" lokasiTempat="Icon 1" namaTempat="Icon 2" />
                        <Link to="/properti"><Button text="Tampilkan Semua" /></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default rekomendasiHome;