import React from 'react'

const produkList = (props) => {
    return (
        <div>
            <div className="produk">
                <div className="imageProduk">
                    <img src={"" + props.img} alt="img" />
                    <div className="detail-info">
                        <h5 className="jenis-properti">Kontrakan</h5>
                        <h5 className="nama-properti">Kontrakan</h5>
                        <a href="#" className="link-properti">Detail<i className="fas fa-angle-right" /></a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default produkList