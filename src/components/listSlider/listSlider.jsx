import React, { Component, Fragment } from 'react'
import Slider from "react-slick"

import Produk from "./produk/produkList"
import Title from '../title/title'

import './listSlider.css'

class listSlider extends Component {
    render() {
        const settings = {
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: false,
            dots: true,
            responsive: [{
                breakpoint: 991.98,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 767.98,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 575.98,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        }

        return (
            <Fragment>
                <div className="jumbotron jumbotron-fluid listprok" style={{ paddingBottom: 4 + 'em' }}>
                    <div className="container">
                        <Title title="Patnership dengan dakon" subTitle="Daftar unggulan rekomendasi dakon" my={1} mx={.5} />
                        <Slider {...settings}>
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                            <Produk img="img/background/img8.jpg" />
                        </Slider>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default listSlider