import React from 'react'

import './buttonDakon.css'

const buttonDakon = (props) => {
    return (<button type="button" className="btn btn-dakon">{props.text}</button>)
}
buttonDakon.defaultProps = {
    text: 'Button'
}
export default buttonDakon