import React, { Component } from 'react'
import Form from './form/form'

import './baner.css'

class baner extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid baner">
                <div className="container">
                    <Form />
                    <div id="carousel" className="carousel slide row align-items-center" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <div className="col-md-12">
                                    <h1>Dakon</h1>
                                    <p>layanan keuangan yang membantu mahasiswa untuk memenuhi kebutuhan tempat tinggal. Dakon bisa dicicil hingga 11 bulan.</p>
                                </div>
                            </div>
                            <div className=" carousel-item">
                                <div className="col-md-12">
                                    <h1>Dakon</h1>
                                    <p>layanan keuangan yang membantu mahasiswa untuk memenuhi kebutuhan tempat tinggal. Dakon bisa dicicil hingga 11 bulan.</p>
                                </div>
                            </div>
                            <div className=" carousel-item">
                                <div className="col-md-12">
                                    <h1>Dakon</h1>
                                    <p>layanan keuangan yang membantu mahasiswa untuk memenuhi kebutuhan tempat tinggal. Dakon bisa dicicil hingga 11 bulan.</p>
                                </div>
                            </div>
                        </div>
                        <ol className="carousel-indicators">
                            <li data-target="#carousel" data-slide-to={0} className="active" />
                            <li data-target="#carousel" data-slide-to={1} />
                            <li data-target="#carousel" data-slide-to={2} />
                        </ol>
                    </div>
                </div>
            </div>
        );
    }
}

export default baner;