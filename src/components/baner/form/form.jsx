import React from 'react';
const form = () => {
    return (
        <div className="carikon">
            <div className="card">
                <div className="card-header">
                    cari kos atau kontrakan?
                </div>
                <div className="card-body">
                    <blockquote className="blockquote mb-0">
                        <p>Dapatkan infonya dan langsung bisa Melakukan cicilan di Dakon.</p>
                    </blockquote>
                    <form>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1">@</span>
                            </div>
                            <input type="text" className="form-control" placeholder="Nama kosan atau kontrakan" aria-label="nama" aria-describedby="basic-addon1" />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1">@</span>
                            </div>
                            <input type="text" className="form-control" placeholder="Alamat lokasi" aria-label="lokasi" aria-describedby="basic-addon1" />
                        </div>
                        <button type="button" className="btn btn-dakon btn-block">Cari</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default form;