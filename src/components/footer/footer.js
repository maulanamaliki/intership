import React, { Component } from 'react';

import './footer.css'

const footer = () => {
    return (
        <div className="footer">
            <div className="container" style={{ marginBottom: 3 + 'em' }}>
                <div className="row">
                    <div className="col-md-4">
                        <div className="titleFooter">
                            <h4>Tentang Dakon</h4>
                        </div>
                        <div className="detailFooter" style={{ marginBottom: 2 + 'em' }}>
                            sebuah website yang dapat membantu anda untuk mencari kontrakan yang sesuai dengan anda inginkan.
                        </div>
                        <div className="detailFooter">
                            Dengan mengunakan DAKON anda dapat mengajukan cicilan sesuai dengan dana yang anda miliki.
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="titleFooter">
                            <h4>Patnersip dakon</h4>
                        </div>
                        <div className="detailFooter img">
                            <div className="card" style={{ maxHeight: 65 }}>
                                <div className="row no-gutters">
                                    <div className="col-3">
                                        <img src="/img/background/img5.jpg" />
                                    </div>
                                    <div className="col-9">
                                        <div className="card-body">
                                            <h5 className="card-title">Alice Liddel</h5>
                                            <p className="card-text">Bootstrap, etc.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="detailFooter img">
                            <div className="card" style={{ maxHeight: 65 }}>
                                <div className="row no-gutters">
                                    <div className="col-3">
                                        <img src="/img/background/img5.jpg" />
                                    </div>
                                    <div className="col-9">
                                        <div className="card-body">
                                            <h5 className="card-title">Alice Liddel</h5>
                                            <p className="card-text">Bootstrap, etc.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="detailFooter img">
                            <div className="card" style={{ maxHeight: 65 }}>
                                <div className="row no-gutters">
                                    <div className="col-3">
                                        <img src="/img/background/img5.jpg" />
                                    </div>
                                    <div className="col-9">
                                        <div className="card-body">
                                            <h5 className="card-title">Alice Liddel</h5>
                                            <p className="card-text">Bootstrap, etc.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="titleFooter">
                            <h4>Kontak</h4>
                        </div>
                        <div className="detailFooter">
                            <div className="kontak"><i className="fas fa-phone fa-rotate-90" aria-hidden="true"></i> +62 8768 78678</div>
                            <div className="kontak"><i className="fas fa-envelope " aria-hidden="true"></i> support@dakon.com</div>
                            <div className="kontak"><i className="fas fa-desktop " aria-hidden="true"></i> dakon.go</div>
                        </div>

                        <div className="titleFooter" style={{ marginTop: 1 + 'em' }}>
                            <h4>Kontak</h4>
                        </div>
                        <div className="detailFooter ss">
                            <div className="sos"><i class="fas fa-rss" aria-hidden="true"></i></div>
                            <div className="sos"><i class="fab fa-facebook-f " aria-hidden="true"></i></div>
                            <div className="sos"><i class="fab fa-twitter " aria-hidden="true"></i></div>
                            <div className="sos"><i class="fab fa-instagram " aria-hidden="true"></i></div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="footer-copyright text-center">
                <div className="container">
                    <p>Copyright 2020 | Dakon. All Rights Reserved.</p>
                </div>
            </div>

        </div>
    );
}

export default footer;