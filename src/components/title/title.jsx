import React from 'react'

import './title.css'

const title = (props) => {
    return (
        <div className="row">
            <div className="col-12 title" style={{
                marginTop: props.my + 'em',
                marginBottom: props.my + 'em',
                marginLeft: props.mx + 'em',
                marginRight: props.mx + 'em',
                textAlign: props.align
            }} >
                <h1>{props.title}</h1>
                <h5>{props.subTitle}</h5>
            </div>
        </div>
    );
}
title.defaultProps = {
    mx: 0,
    my: 0,
    align: 'left'
}
export default title